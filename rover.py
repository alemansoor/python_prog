#!/usr/bin/python

import unittest

class Rover:
    ''' Class to simulate rovers moving on a two-dimensional grid
        Author: Ale Mansoor
	Date: 02/02/2017
    '''
        

    def __init__(self, x = 0, y = 0, heading = ''):
        ''' Function: Rover object constructor
            Args: x coordinate, y coordinate, heading (N, S, E, W)
        '''

        # Set current position
        self._set_position(x, y, heading)

    def _set_position(self, x, y, heading):
        ''' Function: Set position and heading 
 	    Args: x coordinate, y coordinate, heading (N, S, E, W)
        '''

        self.current = { 'x' : x, 'y' : y, 'heading' : heading }

    def move(self):
        ''' Function: Move one position in direction of current heading  
        '''

        if self.current['heading'] == 'N':
            self.current['y'] += 1
        elif self.current['heading'] == 'S':
            self.current['y'] -= 1
        elif self.current['heading'] == 'E':
            self.current['x'] += 1
        elif self.current['heading'] == 'W':
            self.current['x'] -= 1

        # Make sure we are within terrain boundary
        if self.current['x'] > Rover.boundary['x']:
            self.current['x'] = Rover.boundary['x']
        elif self.current['x'] < Rover.initial['x']:
            self.current['x'] = Rover.initial['x']

        if self.current['y'] > Rover.boundary['y']:
            self.current['y'] = Rover.boundary['y']
        elif self.current['y'] < Rover.initial['y']:
            self.current['y'] = Rover.initial['y']

    def rotate(self, direction):
        ''' Function: Rotate left or right by 90 degrees
 	    Args: direction as L or R
        '''

        self.current['heading'] = Rover.ROTATE[self.current['heading']][direction]

    def output(self):
        ''' Function: Generate rover output
            Return: output
        '''

        output = '%d %d %s' % (self.current['x'], 
                               self.current['y'], 
                               self.current['heading'])
        return output

    @staticmethod
    def get_lines(filename='input.txt'):
        ''' Function: static method to get lines from input file
 	    Args: Input file name , defaults to default input file
        '''

        f = None
        lines = []
        try:
            f = open(filename, 'r')
            lines = f.readlines()
        except Exception, e:
            print 'File Exception: %s -%s' % (filename, str(e))
        finally:
            if f is not None:
                f.close()
                f = None

        return lines

    @staticmethod
    def set_boundary(x, y):
        ''' Function: static method to set boundary for rovers
 	    Args: x coordinate, y coordinate, heading (N, S, E, W)
        '''

        # Rotations
        Rover.NORTH_LR = { 'L' : 'W', 'R' : 'E' }
        Rover.SOUTH_LR = { 'L' : 'E', 'R' : 'W' }
        Rover.EAST_LR  = { 'L' : 'N', 'R' : 'S' }
        Rover.WEST_LR  = { 'L' : 'S', 'R' : 'N' }
        Rover.ROTATE = { 'N' : Rover.NORTH_LR,
                           'S' : Rover.SOUTH_LR,
                           'E' : Rover.EAST_LR,
                           'W' : Rover.WEST_LR
                       }

        Rover.boundary = { 'x' : x, 'y' : y }
        Rover.initial = { 'x' : 0, 'y' : 0 }

def make_rovers():
    ''' Function: Function to make rovers  based on input
        Returns: List of rovers
    '''
    # Read input file
    lines = Rover.get_lines()

    # Process first line from input file to set boundary
    if len(lines) >= 1:
        boundary_line = lines[0].split()

        # Set boundary and consume the line
        Rover.set_boundary(int(boundary_line[0]), int(boundary_line[1]))
        del lines[0] 

    # Process remaining input lines
    rover = None
    rovers = []
    count = 0
    for line in lines:

        count += 1
        line.rstrip('\n')

        # First line contains position, next line contains moves or rotates
        # each rover gets two lines of input

        if (count % 2) == 1:

            position = line.split()

            # Make a new rover
            rover = Rover(int(position[0]), 
                          int(position[1]), 
                          position[2])

            # Accumulate rovers for consolidated output
            rovers.append(rover)
        else:

            # Process moves
            for cmd in line:
                if cmd == 'M':	# Move in current heading
                    rover.move()
                elif cmd in Rover.ROTATE['N'].keys(): # Rotate in-place
                    rover.rotate(cmd)

    return rovers

def rovers_output(rovers):
    ''' Function: Function to generate all rovers output
        Args: list of all rovers
        Return: All rovers output
    '''

    output = ''
    for rover in rovers:
        out = rover.output() + '\n'
        output += out

    return output

class TestRover(unittest.TestCase):
    ''' Class to test rovers
    '''

    def test_rover(self):
        ''' Test rover creation and output
	'''

        TestRover.expected_output = '''1 3 N\n5 1 E\n'''
        
        # Create rovers
        rovers = make_rovers()
        self.assertEqual(len(rovers), 2)

        # Create rovers output
        output = rovers_output(rovers)
        self.assertEqual(output, TestRover.expected_output)

        # Finally display output to screen
        print '%s' % output 
        
def main():
    ''' Main program entry point
    '''

    # Test rovers are correctly generated
    unittest.main()
             
if __name__ == "__main__":
    main()
